# totem-meccano

Meccano is the (first) testnet runtime for Totem.

This repo contains functionality ported from the Proof of Concept built from the template node. 

The runtime will eventually be migrated to a full Substrate runtime to be launched as the Totem Mechano testnet.