use support::{decl_module, decl_storage, decl_event, StorageValue, StorageMap, dispatch::Result};
use system::ensure_signed;

use parity_codec::{Encode, Decode};
// use runtime_primitives::traits::{Hash};
// use system::{self, ensure_signed};
// use balances;
use rstd::prelude::*;

pub trait Trait: system::Trait {
	// TODO: Add other types and constants required configure this module.

	type Event: From<Event<Self>> + Into<<Self as system::Trait>::Event>;
}

pub type ProjectHash = Vec<u8>; // Reference supplied externally
pub type BlockNumber = u64; // Blocknumber 
pub type NumberOfBlocks = u64; // Quantity of blocks determines the passage of time
pub type StatusOfTimeClaim = u16; // (posted, resubmitted, accepted, rejected, invoiced, settled)
pub type PostingPeriod = u16; // Not calendar period, but fiscal periods 1-15 (0-14)
pub type StatusOfProject = u16; // (open, closed, cancelled)

#[derive(PartialEq, Eq, Copy, Clone, Encode, Decode, Default)]
#[cfg_attr(feature = "std", derive(Debug))]
pub struct BlocksForAddressProject<NumberOfBlocks,BlockNumber> {
	pub total_blocks: NumberOfBlocks, 
	pub first_seen_block: BlockNumber  
}

#[derive(PartialEq, Eq, Copy, Clone, Encode, Decode, Default)]
#[cfg_attr(feature = "std", derive(Debug))]
pub struct Timekeeper<NumberOfBlocks, StatusOfTimeClaim,PostingPeriod,BlockNumber> {
	pub total_blocks: NumberOfBlocks, 
    pub status: StatusOfTimeClaim,
    pub posting_period: PostingPeriod,
	pub end_block: BlockNumber
}

#[derive(PartialEq, Eq, Copy, Clone, Encode, Decode, Default)]
#[cfg_attr(feature = "std", derive(Debug))]
pub struct ProjectStatus<AccountId,StatusOfProject,BlockNumber> {
	pub owner: AccountId, 
	pub status: StatusOfProject, 
	pub last_action_block_number: BlockNumber  
}
// It is recognised that measurements of time periods using block numbers as a timestamp is not the recommended approach 
// due to significant time-drift over long periods of elapsed time. 

// This module however uses number of blocks as a time measurement (with 1 block equivalent to approximately 5 seconds)
// on the basis that the employee's working time measurement segments do not present a 
// significant calculation risk when measuring and capturing relatively small amounts of booked time.
// The blocktime therefore behaves similar toi a stopwatch for timekeeping.

// It should be noted that validators timestamp each new block with the "correct" timestamp, which can be retrieved
// when needed to give a precise point in time for accounting entries.

decl_storage! {
	trait Store for Module<T: Trait> as TimekeepingModule {        
        // This books the total number of blocks (blocktime) for a given project. 
        // It collates all time booked by all participants.
        pub TotalBlocksPerProject get(total_blocks_per_project): map ProjectHash => Option<NumberOfBlocks>;

        // This records the total amount of blocks booked per address, per project. 
        pub TotalBlocksPerProjectPerAddress get(total_blocks_per_project_per_address): map (T::AccountId,ProjectHash) => Option<BlocksForAddressProject<NumberOfBlocks,BlockNumber>>;

        // This records the amount of blocks per address, per project, per entry. // start block number can be calculated. Only accepted if an end block number is given in the transaction as this is the service rendered date.
        //    .map(Address, Project Hash, End Block number => number of blocks, StatusOfHoursClaim (posted, resubmitted, accepted, rejected, invoiced, settled), posting-period)
        pub TimeRecord get(time_record): map (T::AccountId, ProjectHash, BlockNumber) => Option<Timekeeper<NumberOfBlocks,StatusOfTimeClaim,PostingPeriod,BlockNumber>>;

        // overall hours worked on all projects for a given address
        pub TotalBlocksPerAddress get(total_blocks_per_address): map T::AccountId => Option<NumberOfBlocks>; 

        // When did the project first book time (blocknumber = first seen block nuimber)
        pub ProjectFirstSeen get(project_first_seen): map ProjectHash => Option<BlockNumber>;

        // What is the project status, when did it change?
        // Project Hash => StatusOfProject (open, closed, cancelled), last action blocknumber    
        pub ProjectState get(project_state): map ProjectHash => Option<ProjectStatus<T::AccountId,StatusOfProject,BlockNumber>>;
	}
}

decl_module! {
	pub struct Module<T: Trait> for enum Call where origin: T::Origin {
		fn deposit_event<T>() = default;

		pub fn do_something(origin, something: u32) -> Result {
			let who = ensure_signed(origin)?;

			// <Something<T>>::put(something);

			Self::deposit_event(RawEvent::SomethingStored(something, who));
			Ok(())
		}
	}
}

decl_event!(
	pub enum Event<T> where AccountId = <T as system::Trait>::AccountId {
		SomethingStored(u32, AccountId),
	}
);

/// tests for this module
#[cfg(test)]
mod tests {
	use super::*;

	use runtime_io::with_externalities;
	use primitives::{H256, Blake2Hasher};
	use support::{impl_outer_origin, assert_ok};
	use runtime_primitives::{
		BuildStorage,
		traits::{BlakeTwo256, IdentityLookup},
		testing::{Digest, DigestItem, Header}
	};

	impl_outer_origin! {
		pub enum Origin for Test {}
	}

	// For testing the module, we construct most of a mock runtime. This means
	// first constructing a configuration type (`Test`) which `impl`s each of the
	// configuration traits of modules we want to use.
	#[derive(Clone, Eq, PartialEq)]
	pub struct Test;
	impl system::Trait for Test {
		type Origin = Origin;
		type Index = u64;
		type BlockNumber = u64;
		type Hash = H256;
		type Hashing = BlakeTwo256;
		type Digest = Digest;
		type AccountId = u64;
		type Lookup = IdentityLookup<Self::AccountId>;
		type Header = Header;
		type Event = ();
		type Log = DigestItem;
	}
	impl Trait for Test {
		type Event = ();
	}
	type TimekeepingModule = Module<Test>;

	// This function basically just builds a genesis storage key/value store according to
	// our desired mockup.
	fn new_test_ext() -> runtime_io::TestExternalities<Blake2Hasher> {
		system::GenesisConfig::<Test>::default().build_storage().unwrap().0.into()
	}

	#[test]
	fn it_works_for_default_value() {
		with_externalities(&mut new_test_ext(), || {
			assert_ok!(TimekeepingModule::do_something(Origin::signed(1), 42));
			assert_eq!(TimekeepingModule::something(), Some(42));
		});
	}
}
