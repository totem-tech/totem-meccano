// Copyright 2018/2019 Chris D'Costa
// This file is part of Totem Live Accounting.
// Author Chris D'Costa email: chris.dcosta@totemaccounting.com

// Totem is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Totem is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Totem.  If not, see <http://www.gnu.org/licenses/>.

use parity_codec::{Encode, Decode};
use runtime_primitives::traits::{Hash};
use system::{self, ensure_signed};
use balances;
use rstd::prelude::*;

use support::{decl_module, decl_storage, decl_event, StorageValue, StorageMap, dispatch::Result};

// Every posting to the accounting ledger is said to be a claim, with the possibility of a response
// In this module we handle various claims processes and storage types. 
// It is not intended that the blockchain storage be used for non-state chage information and there 
// are limited use cases for storing other non-accounting data


// Storage of entire claim 
#[derive(PartialEq, Eq, Clone, Encode, Decode)]
#[cfg_attr(feature = "std", derive(Debug))]
pub struct Claim<Balance, 
				 AccountId, 
				 Hash, 
				 Boolean, 
				 DocumentReference, 
				 QuantityBalance,
				 Currency, 
				//  TransactionEventType,
				//  TransactionType,
				 ActionType
				//  ,
				//  AccountingReference
				 > {
	pub claimant: AccountId,
	pub respondent: AccountId,
	pub amount: Balance,
	pub tax_office_account: AccountId,
	pub tax_amount: Balance,
	pub document_text: DocumentReference,
	pub payer: AccountId,
	pub quantity: QuantityBalance,
	pub transaction_payee_currency: Currency,
	pub transaction_payer_currency: Currency,
	pub tax_office_currency: Currency,
	// pub transaction_event_type: TransactionEventType,
	// pub transaction_type: TransactionType,
	pub action_type: ActionType,
	// pub accounting_reference: AccountingReference,
	pub doc_hash: Hash,
	pub settled: Boolean,
}

// Simple account balance structure. Balances can be negative. 
#[derive(PartialEq, Eq, Clone, Encode, Decode)]
#[cfg_attr(feature = "std", derive(Debug))]
pub struct AccountMap<AccountId, AccountBalance> {
	pub account: AccountId, 
	pub account_balance: AccountBalance,  
}

// Backend storage types
pub type ClaimIndex = u64; // every item logged by claim number
pub type AccountBalance = i64; // accounting balance not a crypto balance. Can be negative. 
pub type QuantityBalance = u64; // no negative quantity balances. 

pub type LedgerAccountNumber = u64; // A number which identifies which ledger is to be updated.
pub type Boolean = bool;

// some text from the front-end
pub type DocumentReference = Vec<u8>; // External data. //TODO limit the size of the data stored


// Possible temporary definitions referenced by integer.  
pub type CurrencyNumber = u16; // A refernce number which identifies the currency
pub type TransactionEventType = u16; //
pub type TransactionType = u16; //
pub type ActionType = u16; // 
pub type AccountingReference = u32; //

// Mapping only known to front end.
pub type BankAccountIndexNumber = u32;
pub type CryptoNetworkIndex = u32;

#[derive(PartialEq, Eq, Clone, Encode, Decode)]
#[cfg_attr(feature = "std", derive(Debug))]
pub struct Stock<Hash, QuantityBalance, AccountBalance> {
	pub reference: Hash, // Stock Reference
	pub qty: QuantityBalance,  // Qty of stock on hand
	pub amount: AccountBalance, // value of stock on hand
}

pub trait Trait: system::Trait {
	// TODO: Add other types and constants required configure this module.

	/// The overarching event type.
	type Event: From<Event<Self>> + Into<<Self as system::Trait>::Event>;
}

decl_storage! {
	trait Store for Module<T: Trait> as TotemModule {
	 		// Simple Claim index storage. Getter returns the latest ClaimIndex Number
            pub ClaimsCount get(claims_count): Option<ClaimIndex>;

            // Return a vector array of claim numbers associated with any account
            pub ClaimsByAccount get(claim_index_by_account): map T::AccountId => Vec<ClaimIndex>;

            // Return a vector array of ledgers that have been populated for an account
            // This is an unsorted list of accounts used by the UI to read the general ledger
            pub GLAccountByAccount get(gl_account_by_account): map T::AccountId => Vec<LedgerAccountNumber>;

            // This is a register for all Claims. 
            // Returns the claim details (see struct) by claim index.
            pub ClaimsByClaimIndex get(claims_by_claim_index): map ClaimIndex => Option<Claim<AccountBalance, 
																					   		  T::AccountId, 
																							  T::Hash, 
																							  Boolean, 
																							  DocumentReference, 
																							  QuantityBalance,
																							  Currency, 
																							//   TransactionEventType,
																							//   TransactionType,
																							  ActionType
																							//   ,
																							//   AccountingReference
																							  >>;

            // ************ General Ledger Accounts *****************************/
            // These Accounts set to zero at start of new financial year
            // Also known as the nominal ledger.

            // Gets the balance of of General (aka Nominal) Ledger Accounts by Address
            // Ledger Account identified by a specific index number associated with that account
            // The number range of these accounts is known publically (see documentation).
            pub GeneralLedger get(general_ledger_balance): map (T::AccountId, LedgerAccountNumber) => AccountBalance; 

            // TODO
            // Intended to get the qty and value of stock assets by stock owner Address
            // Returns the vector array of all stock (see struct) for an Address
			// Will probably need to integrate the Assets module, although will also need Unit of Measure 
			// to deal with fractional qty on front end
            pub GLStockAccount get(stock_balance): map T::AccountId => Vec<Stock<T::Hash, QuantityBalance, AccountBalance>>;

            // ************ VAT Accounts *************************/
            // Sales Tax account functions for both seller and buyer and their respective tax accounts
            // getter provides balance by tax account for the claimant or respondent. 
            // create a tuple in the format (claimantAccountID, taxAccountID) to access
            // Behaves like a VAT control account as the balance is what is owed to/by Tax Jurisdiction
            // These accounts may start with an opening balance, and are reset to zero on settlement of taxes
            pub GLVATAccount get(vat_balance): map (T::AccountId, T::AccountId) => AccountBalance;

            // ************ Control Accounts *********************/
            // These accounts start with an opening balance, and continue forever tracking balances
            pub GLPurchasingControl get(purchase_control_balance) : map T::AccountId => AccountBalance;
            pub GLSalesControl get(sales_control_balance) : map T::AccountId => AccountBalance;


            // ************ Memorandum Accounts - live forever ******************/
            // ************ Purchase Ledger Memorandum Account ******************/
            // get balances of vendors(s) by account
            // access using (purchaserAccountID, sellerAccountID)
            pub PLVendorAccount get(vendor_balance): map (T::AccountId, T::AccountId) => AccountBalance;

            // ************ Sales Ledger Memorandum Accounts ********************/
            // get by customer(s) for Address and balances
            // getter provides balance by claimant account for the customer (respondent). 
            // create a tuple in the format (claimantAccountID, respondentAccountID) to access		
            pub SLCustomerAccount get(customer_balance): map (T::AccountId, T::AccountId) => AccountBalance;

            // ************ Banking Accounts ********************/
            // This is specifically to handle fiat balances, and has to be managed by user in v1.0.
            // Bank accounts just use an incremental index set in the front end. 
            pub BKSpendAccount get(spend_balance): map (T::AccountId, BankAccountIndexNumber) => AccountBalance;
            
			// Hack to temporarily map balances from other crypto networks! Not directly connected to crypto networks!
			pub BKCryptoAccount get(crypto_balance): map (T::AccountId, T::Hash, CryptoNetworkIndex) => AccountBalance;

            // TODO - possibly in v2.0
            // Settlement using crypto employs the balance on the account itself using the SDR exchange rate.

		// Just a dummy storage item. 
		// Here we are declaring a StorageValue, `Something` as a Option<u32>
		// `get(something)` is the default getter which returns either the stored `u32` or `None` if nothing stored
		Something get(something): Option<u32>;
	}
}

decl_module! {
	/// The module declaration.
	pub struct Module<T: Trait> for enum Call where origin: T::Origin {
		// Initializing events
		// this is needed only if you are using events in your module
		fn deposit_event<T>() = default;

		// Just a dummy entry point.
		// function that can be called by the external world as an extrinsics call
		// takes a parameter of the type `AccountId`, stores it and emits an event
		pub fn do_something(origin, something: u32) -> Result {
			// TODO: You only need this if you want to check it was signed.
			let who = ensure_signed(origin)?;

			// TODO: Code to execute when something calls this.
			// For example: the following line stores the passed in u32 in the storage
			<Something<T>>::put(something);

			// here we are raising the Something event
			Self::deposit_event(RawEvent::SomethingStored(something, who));
			Ok(())
		}

		pub fn process_claim(
			origin, 
			// _origin_pubkey: T::Hash,
			respondent: T::AccountId,
			_payer: T::AccountId,
			amount: AccountBalance,
			_quantity: QuantityBalance,
			_transaction_payee_currency: Currency,
			_transaction_payer_currency: Currency,
			tax_office_account: T::AccountId,
			tax_amount: AccountBalance,
			_tax_office_currency: Currency,
			// _transaction_event_type: TransactionEventType,
			// _transaction_type: TransactionType,
			_action_type: ActionType,
			// _accounting_reference: AccountingReference,
			claim_number: ClaimIndex,
			document_text: DocumentReference
			) -> Result {
			// Initial checks
			let claimant = ensure_signed(origin)?;

			let amount: AccountBalance = amount.into();
			let tax_office_account: T::AccountId = tax_office_account.clone();
			let tax_amount: AccountBalance = tax_amount.into();
			let doc_hash: T::Hash = document_text.using_encoded(<T as system::Trait>::Hashing::hash);
			let entered_claim_number: ClaimIndex = claim_number.into();

			let mut c: ClaimIndex = 0;
			let default_claim_nr: ClaimIndex = 0;
		    
			if <ClaimsCount<T>>::exists() { 
				// There are existing claims, therefore get the last claim number. If not then it is the default value of 0.
				// perform check against number that was entered in UI.
				c = Self::claims_count().ok_or("Storage Read Error: cannot get claim count")?; // Should not fail but unwrap
									
			};

			let last_claim_nr: ClaimIndex = c.into();

			if entered_claim_number > default_claim_nr && entered_claim_number <= last_claim_nr {
				// info!("TOTEM: claim_number > 0 && claim_number <= last_claim_nr");
				// a change to an existing claim
				// use the claim number to check that the sender is the claimant fot this claim number. If not then cannot amend. End. 
				let _claims_vec = Self::claim_index_by_account(&claimant)
					.into_iter()
					.find(| &x| x == entered_claim_number)
					.ok_or("This claimant doesn't have any claims to change!")?;	 
				
				// TODO Check the Transaction Event Type to determine if it is an amend, withdrawal or writeoff.
				// If withdraw or write off, reverse out entries. Write off post to appropriate account.
				
				// If amend then
				// TODO Decide which elemets are changeable, and which are not.

				// Do not forget that the ledgers need to be updated with any changes to amounts.
				// An amendment
				// First reverse the existing postings on the ledger, then repost with the new values
				// If this is a credit note	post as amendment.


			} else if entered_claim_number == default_claim_nr {
				// info!("TOTEM: claim_number = 0");				
				// This is a new claim request.

				// Store the next claim counter
				<ClaimsCount<T>>::put(&c + 1);

				// Associate the claim to the claimant and respondent
				<ClaimsByAccount<T>>::mutate(&claimant, |v| v.push(&c + 1));
				<ClaimsByAccount<T>>::mutate(&respondent, |v| v.push(&c + 1));

				let claim_data = Claim {
					claimant: claimant,  
					respondent: respondent,
					amount: amount,
					tax_office_account: tax_office_account, 
					tax_amount: tax_amount,
					document_text: document_text,
					payer: _payer,
					quantity: _quantity,
					transaction_payee_currency: _transaction_payee_currency,
					transaction_payer_currency: _transaction_payer_currency,
					tax_office_currency: _tax_office_currency, 
					// transaction_event_type: _transaction_event_type,
					// transaction_type: _transaction_type,
					action_type: _action_type,
					// accounting_reference: _accounting_reference,
					doc_hash: doc_hash,
					settled: false,
				};

				<ClaimsByClaimIndex<T>>::insert(c, &claim_data);		

				let credit_amount: AccountBalance = -claim_data.amount;
				let total_amount: AccountBalance = claim_data.amount + claim_data.tax_amount;
				let credit_total_amount: AccountBalance = -total_amount;
				let credit_tax_amount: AccountBalance = -claim_data.tax_amount;
				
				// Dummy placeholder values for now.
				// Full list of accounts are in JSON for the front end
				let general_ledger_account: LedgerAccountNumber = 900000000000000; // eg Office Expense
				let stock_value_account: LedgerAccountNumber = 300000000000000; // eg Stock account by product 

				// create tuple for accessing the sales tax balance.
				let tax_relations_claimant = (claim_data.claimant.clone(), claim_data.tax_office_account.clone());
				let tax_relations_respondent = (claim_data.respondent.clone(), claim_data.tax_office_account.clone());
				let customer_relations = (claim_data.claimant.clone(), claim_data.respondent.clone());
				let vendor_relations = (claim_data.respondent.clone(), claim_data.claimant.clone());
				
				
				let claimant_gl_account = (claim_data.claimant.clone(), stock_value_account.clone()); 
				let respondent_gl_account = (claim_data.respondent.clone(), general_ledger_account.clone());

				// info!("TOTEM: Start of Ledger Updates");
				<GLVATAccount<T>>::mutate(tax_relations_claimant, |v| *v += credit_tax_amount);
				<GLVATAccount<T>>::mutate(tax_relations_respondent, |v| *v += claim_data.tax_amount);

				<GeneralLedger<T>>::mutate(&claimant_gl_account, |v| *v += credit_amount);
				<GeneralLedger<T>>::mutate(&respondent_gl_account, |v| *v += claim_data.amount);

				<GLSalesControl<T>>::mutate(&claim_data.claimant, |v| *v += total_amount);
				<SLCustomerAccount<T>>::mutate(customer_relations, |v| *v += total_amount);

				<GLPurchasingControl<T>>::mutate(&claim_data.respondent, |v| *v += credit_total_amount);
				<PLVendorAccount<T>>::mutate(vendor_relations, |v| *v += credit_total_amount);

				// Event
				// Self::deposit_event(RawEvent::SomethingStored(something, who));

			} else {
				// info!("TOTEM: Claim number too big!");	
				// this claim number is greater than the existing claims. Ignore and end.
				// hack because its TODO
				let _claims_vec = Self::claim_index_by_account(&claimant)	
					.into_iter()
					.find(|&x| x == entered_claim_number)
					.ok_or("This claim has not been created yet!")?;
			}
			
			Ok(())
		}
	}
}

decl_event!(
	pub enum Event<T> where AccountId = <T as system::Trait>::AccountId {
		// Just a dummy event.
		// Event `Something` is declared with a parameter of the type `u32` and `AccountId`
		// To emit this event, we call the deposit funtion, from our runtime funtions
		SomethingStored(u32, AccountId),
	}
);

/// tests for this module
#[cfg(test)]
mod tests {
	use super::*;

	use runtime_io::with_externalities;
	use primitives::{H256, Blake2Hasher};
	use support::{impl_outer_origin, assert_ok};
	use runtime_primitives::{
		BuildStorage,
		traits::{BlakeTwo256, IdentityLookup},
		testing::{Digest, DigestItem, Header}
	};

	impl_outer_origin! {
		pub enum Origin for Test {}
	}

	// For testing the module, we construct most of a mock runtime. This means
	// first constructing a configuration type (`Test`) which `impl`s each of the
	// configuration traits of modules we want to use.
	#[derive(Clone, Eq, PartialEq)]
	pub struct Test;
	impl system::Trait for Test {
		type Origin = Origin;
		type Index = u64;
		type BlockNumber = u64;
		type Hash = H256;
		type Hashing = BlakeTwo256;
		type Digest = Digest;
		type AccountId = u64;
		type Lookup = IdentityLookup<Self::AccountId>;
		type Header = Header;
		type Event = ();
		type Log = DigestItem;
	}
	impl Trait for Test {
		type Event = ();
	}
	type Totem = Module<Test>;

	// This function basically just builds a genesis storage key/value store according to
	// our desired mockup.
	fn new_test_ext() -> runtime_io::TestExternalities<Blake2Hasher> {
		system::GenesisConfig::<Test>::default().build_storage().unwrap().0.into()
	}

	#[test]
	fn it_works_for_default_value() {
		with_externalities(&mut new_test_ext(), || {
			// Just a dummy test for the dummy funtion `do_something`
			// calling the `do_something` function with a value 42
			assert_ok!(Totem::do_something(Origin::signed(1), 42));
			// asserting that the stored value is equal to what we stored
			assert_eq!(Totem::something(), Some(42));
		});
	}
}
